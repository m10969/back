import functools
import os.path
from abc import ABC, abstractmethod
from typing import Any

import numpy as np
from joblib import load
from loguru import logger
from pydantic import BaseModel


class Model(ABC, BaseModel):
    @staticmethod
    @abstractmethod
    def load_model() -> Any:
        raise NotImplementedError

    @abstractmethod
    def predict(self, x: Any) -> Any:
        raise NotImplementedError

    @abstractmethod
    def predict_proba(self, x: Any) -> Any:
        raise NotImplementedError


class Classifier(BaseModel):
    @staticmethod
    @functools.lru_cache(maxsize=1)
    def load_model() -> Model:
        logger.info('Cargando el modelo')
        clf = load(os.path.join(os.getcwd(), 'models/fraud_detector.pkl'))
        return clf

    def predict(self, data: np.ndarray) -> np.ndarray:
        clf = self.load_model()
        return clf.predict(data)

    def predict_proba(self, data: np.ndarray) -> np.ndarray:
        clf = self.load_model()
        return clf.predict_proba(data)

    class Config:
        arbitrary_types_allowed = True
