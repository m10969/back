from pydantic import BaseModel, conlist


class PredictionRequestDto(BaseModel):
    data: conlist(conlist(float, min_items=2, max_items=2), min_items=1, max_items=1)


class PredictionResponseDto(BaseModel):
    is_fraud: bool
    probability: float
