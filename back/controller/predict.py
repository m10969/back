import numpy as np
from loguru import logger
from pydantic import BaseModel

from back.model.classifier import Classifier
from back.view.prediction import PredictionRequestDto, PredictionResponseDto


class PredictController(BaseModel):
    @staticmethod
    def predict(prediction_request: PredictionRequestDto) -> PredictionResponseDto:
        model = Classifier()
        logger.info(f'Iniciando predicción con datos {prediction_request}')
        data = np.array(prediction_request.data)
        y_pred = model.predict(data)
        y_prob = model.predict_proba(data)
        is_fraud = y_pred[0]
        probability = y_prob[0][1] if is_fraud else y_prob[0][0]

        response = PredictionResponseDto(is_fraud=is_fraud, probability=probability)
        logger.info(f'Se obtuvo la predicción {response}')
        return response
