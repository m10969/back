import uvicorn
from fastapi import FastAPI, Depends

from back.controller.predict import PredictController
from back.view.prediction import PredictionRequestDto, PredictionResponseDto

app = FastAPI()


@app.get("/")
def root():
    return {"hello": "world"}


@app.post('/', response_model=PredictionResponseDto)
async def predict(data: PredictionRequestDto, controller: PredictController = Depends(PredictController)):
    return controller.predict(data)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
