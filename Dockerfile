FROM python:3.9-alpine AS build
COPY . .
RUN pip install -r requirements.txt

FROM build AS run
EXPOSE 8000
CMD ["uvicorn", "back.main:app"]



