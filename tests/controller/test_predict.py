from back.controller.predict import PredictController
from back.view.prediction import PredictionRequestDto


class TestPredictController:
    def test_should_predict_a_non_fraud_given_0_transactions(self):
        controller = PredictController()
        data = PredictionRequestDto(data=[[0, 0]])
        response = controller.predict(data)
        assert response.is_fraud is False

    def test_should_predict_a_fraud_given_3_transactions(self):
        controller = PredictController()
        data = PredictionRequestDto(data=[[3, 3]])
        response = controller.predict(data)
        assert response.is_fraud is True
