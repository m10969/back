# Backend

Implementación de un backend sencillo adaptando el patrón MVC para exponer el modelo de detección de fraudes como un servicio web.

## Ejecución en local

* Instalar [poetry](https://python-poetry.org/)
* Clonar el proyecto
* `poetry shell`
* `poetry install`
* `uvicorn back.main:app --reload`

## Pruebas

### Pruebas unitarias
Para las pruebas unitarias se usa la librería [`pytest`](https://docs.pytest.org/). Las pruebas se ubican en el 
directorio `test/`, ubicado en la raíz del proyecto. Las pruebas pueden ejecutarse usando el comando ``coverage run --omit="test*" -m pytest test -v``

### Cobertura

El porcentaje de cobertura del proyecto puede determinarse usando 
la librería [`coverage`](), ejecutando en la terminal el comando  `coverage report --omit=$HOME/.local/*,/usr/lib/*,test*,/usr/local/lib64*`

Se puede generar un reporte HTML usando `coverage html --omit=$HOME/.local/*,/usr/lib/*,test*`

Si las pruebas se realizan desde una consola `zsh`, incluir como prefijo `noglob`