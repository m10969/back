from sklearn.linear_model import LogisticRegression

from back.model.classifier import Classifier


class TestClassifier:
    def test_should_load_the_model_properly(self):
        model = Classifier.load_model()

        assert model is not None
        assert isinstance(model, LogisticRegression)
